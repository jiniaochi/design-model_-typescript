/**
 * 原型链 组合模式
 * 父类原型-> 原型链模式
 * 父类自有属性-> 构造函数模式
 */
function ParentClass(name,age=10){
    this.name = name;
    this.age = age;
}
ParentClass.prototype.log = function(){
    console.log( `name=${this.name}; age=${this.age}` );
}


function ChildClass(name,adddress){
    ParentClass.call(this,name);
    this.adddress = adddress;
}

ChildClass.prototype = new ParentClass()
ChildClass.prototype.log = function(){
    console.log( `name=${this.name}; adddress=${this.adddress}; age=${this.age}` );
}
ChildClass.prototype.constructor = ChildClass;

let child = new ChildClass('jinxiaochi','GuangDong Shenzhen');
child.log();


