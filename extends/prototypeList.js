/**
 * 1. 原型链的继承
 * 子类的原型等于父类实例。
 */
function ParentClass(name,age=0){
    this.name = name;
    this.age = age;
    console.log( this );
}

ParentClass.prototype.log = function(){
    console.log( `name=${this.name}; age=${this.age}` );
}

function ChildClass(name,adddress){
    // 这里写不行？？ 改变不了 prototype
    ChildClass.prototype = new ParentClass('4856',15);
    this.name = name;
    this.adddress = adddress;
}


// ChildClass.prototype = new ParentClass('4856',15);
// 子类原型特有属性需要后面再设置，尤其是contructor构造函数需要更正
ChildClass.prototype.log = function(){
    console.log( `name=${this.name}; adddress=${this.adddress}; age=${this.age}` );
}
// ChildClass.prototype.constructor = ChildClass; // 不改的话构造函数就是 ParentClass


let child = new ChildClass('asmkdlkkllk','GuangDong Shenzhen');
child.log();
console.log( child instanceof ParentClass );

/**
 * instanceof用于检测构造函数的prototype  属性是否出现在实例对象的原型链上。
 */
console.log( child instanceof Object );
ParentClass.prototype =  Number.prototype;
console.log( new ParentClass('sad',12),ParentClass.prototype, Object.prototype );
console.log( new ParentClass('sad',12) instanceof Number );
// 等于
console.log( Number.prototype.isPrototypeOf(new ParentClass('sad',12)) );// true

