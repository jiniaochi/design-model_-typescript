/**
 * es6继承
 * 
 * super 关键字用于访问对象字面量或类的原型（[[Prototype]]）上的属性，或调用父类的构造函数。
 * super.prop 和 super[expr] 表达式在类和对象字面量任何方法定义中都是有效的。super(...args) 表达式在类的构造函数中有效。
**/

class ParentClass{
    constructor(name,age=15){
        this.name = name;
        this.age = age;
    }

    log(){
        console.log( `name=${this.name}; age=${this.age}` );
    }
}

class ChildClass extends ParentClass{
    constructor(name,address){
        super(name);
        this.name = name;
        this.adddress = address;
    }
    log(){
        super.log() // name=sadnqwkjd; age=15
        console.log( super.name, super.age, super.log ) // undefined undefined [Function: log]; 我猜：name和age被覆盖了，就变成了undefined
        console.log( `name=${this.name}; adddress=${this.adddress}; age=${this.age}` );
    }
}

let child =  new ChildClass('sadnqwkjd','BHASN');
child.log();
console.log( child, Object.getPrototypeOf(child) );