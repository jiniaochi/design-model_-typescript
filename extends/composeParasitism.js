/**
 * 将组合式和寄生式结合起来； 父类自身属性用构造函数继承，父类原型属性用寄生式继承。
 */
function ParentClass(name,age=10){
    this.name = name;
    this.age = age;
}
ParentClass.prototype.log = function(){
    console.log( `name=${this.name}; age=${this.age}` );
}

function ChildClass(name,adddress){
    let obj = Object.create( ParentClass.prototype )
    obj.adddress = adddress;
    // 自身的原型属性就被抹掉了
    obj.log = function(){
        console.log( `name=${this.name}; adddress=${this.adddress}; age=${this.age}` );
    }

    ParentClass.call(obj,name);
    return obj;
}

// 此方法不生效
ChildClass.prototype.log = function(){
    console.log( `name=${this.name}; adddress=${this.adddress}; age=${this.age}` );
}


let child =  new ChildClass('sadnqwkjd','BHASN');
child.log();
Object.getPrototypeOf(child).log.call(child);