/**
 * 构造函数继承
 * 
 * 子类构造函数调用父类构造函数（使用`call`或`apply`方法）。
 */
function ParentClass(name,age=10){
    this.name = name;
    this.age = age;
}
ParentClass.prototype.log = function(){
    console.log( `name=${this.name}; age=${this.age}` );
}

function ChildClass(name,adddress){
    ParentClass.call(this,name);
    this.adddress = adddress;
}

ChildClass.prototype.log = function(){
    console.log( `name=${this.name}; adddress=${this.adddress}; age=${this.age}` );
}

let child = new ChildClass('jinxiaochi','GuangDong Shenzhen');
child.log();