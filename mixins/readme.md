# JavaScript混入（Mixin，多继承；extend）


## js的多态（Polymorphism）

多态，就是对同一种方法的多种调用方式。

```js
/**
 * 多态
 */
function add(...nums){
    let sum = 0, intVal;
    // 如果不传参数的话，就是空数组
    if(!nums.length){
        return sum;
    }

    for(let val of nums){
        intVal = Number(val);
        if(intVal){
            sum += intVal;
        }
    }
    return sum;
}

console.log( add() ); //0 调用方式一
console.log( add(5) ); //5 调用方式二
console.log( add(8,10) ); //18
```

## 封装（encapsulation）

对数据和数据的方法进行聚合操作，让分散的的数据、变量、方法成为一个整体。

