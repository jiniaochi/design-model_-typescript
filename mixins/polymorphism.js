/**
 * 多态
 */
function add(...nums){
    let sum = 0, intVal;
    // 如果不传参数的话，就是空数组
    if(!nums.length){
        return sum;
    }

    for(let val of nums){
        intVal = Number(val);
        if(intVal){
            sum += intVal;
        }
    }
    return sum;
}

console.log( add() ); //0 调用方式一
console.log( add(5) ); //5 调用方式二
console.log( add(8,10) ); //18
