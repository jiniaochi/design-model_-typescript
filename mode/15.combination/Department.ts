/**
 * 公司下属部门(叶子节点)
 */

import { Company } from './Company' 

class Department extends Company{
	
	public add(company:Company):void{
		console.log(`部门不能添加子公司或部门`);
	}
	
	public remove(company:Company):void{
		console.log(`部门不能移除子公司或部门`);
	}
	
	public show():void{
		console.log(`部门--${ this.getName() },编号为 ${ this.getId() },参上!!`);
	}
	
}

export { Department } 
