/**
 * 组合模式：集团(根节点)-分公司(分支节点)-部门(叶子节点) 这种树形结构 适用于 组合模式
 * 公司接口类
 */


abstract class Company{
	
	private name:string;
	private id:number;
	
	public constructor(name:string,id:number){
		this.id = id;
		this.name = name;		
	}
	
	//添加子公司或者部门
	public abstract add(company:Company):void;
	
	//移除子公司或者部门
	public abstract remove(company:Company):void;
	
	//展示本公司子公司和下属部门
	public abstract show():void;
	
	public getName():string{
		return this.name;
	}
	
	public getId():number{
		return this.id;
	}
}

export { Company }
