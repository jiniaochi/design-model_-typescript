/**
 * 具体公司类
 */

import { Company } from './Company'
 
class ConcreteCompany extends Company{
 
	//子公司、部门列表
	private companyList:Company[];
	
	public constructor(name:string,id:number){
		super(name,id);
		this.companyList = new Array();
	}
	
	//添加子公司或者部门
	public add(company:Company):void{
		let index = this.companyList.findIndex(el=>el.getId() == company.getId());
		if(index >= 0){
			console.log(`${company.getName()}的编号${company.getId()} 已经存在，不得重复添加`);
		}else{
			this.companyList.push(company);
		}
	}

	//移除子公司或者部门
	public remove(company:Company):void{
		let index = this.companyList.findIndex(el=>el.getId() == company.getId());
		if(index >= 0){
			this.companyList.splice(index,1);
		}
	}

	//展示本公司子公司和下属部门, 树形节点的先序遍历
	public show():void{
		console.log(`本公司编号为额${this.getId()},全称${this.getName()},包括以下子公司和部门:`);
		this.companyList.forEach(el=>el.show());
	}	 
}

export { ConcreteCompany }