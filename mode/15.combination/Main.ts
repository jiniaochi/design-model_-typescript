/**
 * 组合模式： 将不同类型的对象组合成树形结构的一种模式，是适用于 树形数据结构的 设计模式
 * 组合模式先提取这些对象的公共特征成为抽象类/接口（类关系分析必要的一步）
 * 然后再 将各个不同的派生子类组合起来
 */
import { ConcreteCompany } from './ConcreteCompany'
import { Department } from './Department'

let company = new ConcreteCompany('卡峰集团',1);

company.add(new Department('人力资源部',11));
company.add(new Department('信息技术部',12))
company.add(new Department('采购部',13))

let subCompany = new ConcreteCompany('卡峰子公司',14);
company.add(subCompany);

subCompany.add(new Department('子公司人力资源部门',141));
subCompany.add(new Department('子公司采购部门',142));

company.show();