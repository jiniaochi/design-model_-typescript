/**
 * 中介者类: 
 * 中介者模式: 类A和类B 实例均持有 同一中介者类实例，A、B交互不直接发送消息，而是通过中介者来发送。
 */
import { Boss } from './Boss'
import { Worker } from './Worker'
import { Person } from './Person'

class Intermediary{
	
	private boss:Boss;
	private worker:Worker;
	
	public set(boss:Boss,worker:Worker):void{
		this.boss = boss;
		this.worker = worker;
	}
	
	public notify(person:Person):void{
		if(person.getName() == this.worker.getName()){
			this.boss.answer();
		}else if(person.getName() == this.boss.getName()){
			this.worker.answer();
		}
	}
	
}

export  { Intermediary }