/**
 * 中介者认识的人--抽象类
 */
import { Intermediary } from './Intermediary'

abstract class Person{
	
	protected name:string;
	protected interm:Intermediary;
	
	public constructor(name:string,interm:Intermediary){
		this.name = name;
		this.interm = interm;
	}
	
	public getName():string{
		return this.name;
	}
	
	//提问--求职者实现
	public  question():void{
		
	}
	
	//回答--老板、求职者实现
	public  answer():void{
		
	}
}

export { Person }
