/**
 * 老板类
 */
import { Person } from './Person'
import { Intermediary } from './Intermediary'

class Boss extends Person{
	
	public constructor(name:string,interm:Intermediary){
		super(name,interm);
	}
	
	public answer():void{
		console.log(`老板 ${this.name} 回答求职者: 一个月工资8000-10000，视工作能力而定`);
		this.interm.notify(this);
	}
	
}

export { Boss };
