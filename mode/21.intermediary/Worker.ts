/**
 * 求职者类
 */

import { Person } from './Person'
import { Intermediary } from './Intermediary'

class Worker extends Person{
	
	public constructor(name:string,interm:Intermediary){
		super(name,interm);
	}
	
	public question():void{
		console.log(`求职者 ${this.name}向老板 提问: 请问一个月工资多少?`);
		this.interm.notify(this);
	}
	
	public answer():void{
		console.log(`求职者 ${this.name}向老板 回答: 知道了`);
	}
}

export { Worker }