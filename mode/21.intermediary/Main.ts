/**
 * 中介者模式: 类A和类B 实例均持有 同一中介者类实例，A、B交互不直接发送消息，而是通过中介者来发送。
 * 有点类似适配器模式，但是不同的是中介者模式中的中介者是独立的，不继承/实现 任何其他的类。
 */

import { Intermediary } from './Intermediary'
import { Boss } from './Boss'
import { Worker } from './Worker'

let interm = new Intermediary(), boss = new Boss('王富贵',interm), worker = new Worker('王刚',interm);

interm.set(boss,worker);

worker.question();

