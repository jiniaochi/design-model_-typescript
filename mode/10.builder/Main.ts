/**
 * 构建者模式: 将 一个对象的构建过程拆分为多个步骤， 使相同的步骤但不同的内容创造出的对象不同。
 * 
 * 如下面两个构建者 法拉利构建者、保时捷构建者，但是二者都继承汽车构建者基类，都是安装引擎、安装其他、安装车轮的步骤，
 * 但是二者 操作的内容不同，最后创建的对象内容
 * 
 */

import { FerrariCar } from './FerrariCar';
import { PorscheCar } from './PorscheCar';

new FerrariCar().productCar();
new PorscheCar().productCar();