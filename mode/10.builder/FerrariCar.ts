/**
 * 法拉利汽车 
 */
import Car from './Car';
import { CarBuilder } from './CarBuilder'

class FerrariCar extends CarBuilder{
	
	private car:Car;
	
	public constructor(){
		super()
		this.car = new Car('法拉利',180)
	}
	
	//安装车轮
	protected installWheel():Car{		
		this.car.setWheel()
		return this.car
	}
	
	//安装引擎
	protected installEngine():Car{
		this.car.setEngine()
		return this.car
	}
	
	//安装其他部件
	protected installOthers():Car{
		this.car.setOther()
		return this.car
	}
}

export {FerrariCar}