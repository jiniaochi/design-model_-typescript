
export default class Car{
    private name:String;
    private speed:Number;

    private hasEngine:Boolean = false;
    private hasOther:Boolean = false;
    private hasWheel:Boolean = false;

    constructor(name:String,speed:Number){
        this.name = name
        this.speed = speed
    }

    setEngine(){
        console.log(`${this.name} 组装车轮在上海完成`);
        this.hasEngine = true
    }

    setOther(){
        console.log(`${this.name} 组装引擎在上海完成`);
        this.hasOther = true
    }

    setWheel(){
        console.log(`${this.name} 组装其他组件在上海完成`);
        this.hasWheel = true
    }

    isOK(){
        return this.hasEngine&&this.hasOther&&this.hasWheel
    }

    print(){
        if(this.isOK){
            return `车名：${this.name}, 速度： ${this.speed} km/h`
        }else{
            return '汽车未生产完毕'
        }
    }
}