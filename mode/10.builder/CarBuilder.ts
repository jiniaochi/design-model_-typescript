import Car from "./Car";

/**
 * 汽车构建者 抽象类:
 * 构造者的 优势-- 保证封装性、顺序性
 * public' modifier cannot appear on a module or namespace element. 
 * 								--typescript中public无法修饰class
 */
abstract class CarBuilder{
	
	//组装/生产 出一辆xx牌汽车,需要按照一定顺寻
	public productCar():Car{
		this.installEngine();
		this.installOthers();
		let car = this.installWheel();
		console.log(`汽车组装完毕`, car.print());
		return car;
	};
	
	//安装车轮
	protected abstract installWheel():Car;
	
	//安装引擎
	protected abstract installEngine():Car;
	
	//安装其他部件
	protected abstract installOthers():Car;
}	


export { CarBuilder }
