import Operate from "./Operate";
import State from "./State";
import StartLoading from "./StateLoading";


export default class StartState implements State{
    getStatus(): string {
        console.log('do something of start');
        return `开始状态`
    }
    nextStatus(op:Operate): State {
        console.warn(`进入下一状态`);
        // 被调用方修改调用方： 秒啊
        return op.state = new StartLoading()
    }
}

