import State from "./State";
import StartState from "./StateStart";

export default class Operate{

    private _state:State;
    get state(){
        return this._state
    }
    set state(newState:State){
        this._state = newState
    }

    constructor(){
        this._state = new StartState()
    }

    public doIt(){
        console.log(`现在状态是，${this._state.getStatus()}`);
        this._state.nextStatus(this)
    }

}
