import Operate from "./Operate";
import State from "./State";


export default class StartEnd implements State{
    getStatus(): string {
        console.log('do something of end');
        return `结束状态`
    }
    nextStatus(op:Operate): State {
        console.warn(`已经是结束状态了，不存在下一状态`);
        return this
    }
    
}
