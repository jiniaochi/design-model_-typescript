import State from "./State";
import StateEnd from "./StateEnd";
import Operate from './Operate';


export default class StartLoading implements State{
    getStatus(): string {
        console.log('do something of loading');
        return `进行中状态`
    }
    nextStatus(op:Operate): State {
        console.warn(`进入下一状态`);
        return op.state = new StateEnd()
    }
    
}
