/**
 * 状态模式：对象操作后，自动转化为下一个状态
 * 状态模式通过把各种状态逻辑分布到State子类之间，减少耦合，消除了庞大的if-else分支语句（如这个例子中三个子类就可以使用一个状态变量state配合三个if-else实现）
 * 状态模式将多个if-esle 分布到不同子类，对于不同状态进行不同子类对象的切换。
 * 
 * 当一个对象的行为取决于其状态，且它必须运行时时刻根据状态改变其行为时，可以考虑使用状态模式。
 * 
 * 
 * 【额外注意】
 * 另外还有一个就是，我们一般是调用方对象改变被调用方对象，这里使用到了 被调用方对象改变主动调用方对象（主动方调用时传参this，妙啊）
 */

import Operate from "./Operate";

const operator:Operate = new Operate()

operator.doIt()
operator.doIt()
operator.doIt()
operator.doIt()
