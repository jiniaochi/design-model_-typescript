import Operate from './Operate';
// 状态 接口类

export default interface State{
    getStatus():string;
    nextStatus(op:Operate):State;
}
