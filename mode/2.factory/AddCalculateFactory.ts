
import { AddCal } from "./AddCal";
import { Calculate } from "./Calculate";
import CalculateFactory from "./CalculateFactory";

export default class AddCalculateFactory implements CalculateFactory{

    getCalculateObject(): AddCal {
        return new AddCal()
    }
    
}
