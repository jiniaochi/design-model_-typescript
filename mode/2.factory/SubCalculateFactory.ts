
import { Calculate } from "./Calculate";
import CalculateFactory from "./CalculateFactory";
import { SubCal } from "./SubCal";

export class SubCalculateFactory implements CalculateFactory{
    getCalculateObject(): SubCal {
        return new SubCal()
    }
}
