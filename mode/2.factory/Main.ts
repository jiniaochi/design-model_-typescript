/**
 * 工厂模式：
 * 相对于 之前的简单工厂模式，一个工厂类生产多个 类型对象；
 * 工厂模式 存在多个工厂类继承一个接口/基类，每个工厂类生产的对象都是相同的，如加法工厂生产的是加法对象。
 */
import AddCalculateFactory from "./AddCalculateFactory";
import { SubCalculateFactory } from "./SubCalculateFactory";

let  addCal = new AddCalculateFactory().getCalculateObject(),
	subCal = new SubCalculateFactory().getCalculateObject();

addCal.operate(12,24);
subCal.operate(10,19);


