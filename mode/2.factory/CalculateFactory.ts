import { Calculate } from './Calculate';

export default interface CalculateFactory {
    getCalculateObject():Calculate;
}


