import {Calculate} from './Calculate';
	
class AddCal implements  Calculate{
	
	constructor(){
		console.log(`加法对象被创造`);
	}
	
	operate(num1:number,num2:number):number{
		let res:number = num1 + num2;
		console.log(`相加结果为 ${res}`);
		return res;
	}
}

export {AddCal}
	

