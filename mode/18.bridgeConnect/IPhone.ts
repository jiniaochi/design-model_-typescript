/**
 * 手机品牌：IPhone
 */
import { MobileBrand } from './MobileBrand'

class IPhone implements MobileBrand{
	
	public getBrand():string{
		return 'IPhone手机';
	};
	
}

export { IPhone }
