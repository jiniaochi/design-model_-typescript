/**
 * 桥接模式： 手机品牌类
 */

interface MobileBrand {
	
	//返回手机品牌
	getBrand():string;
		
}

export { MobileBrand }
