/**
 * 桥接模式：将两个独立的体系通过 提取 各自共同的抽象基类，实现不同体系之间的通信，如下面的是 手机软件同手机品牌之间的通信。
 * 将抽象部分（如手机）和实现部分（如手机的各种品牌，如小米和苹果）分离，让他们可以独立的变化；
 * 例如 下面的抽象部分就是手机软件类，而手机游戏、手机邮箱都是实现部分。
 */

import { IPhone } from './IPhone'
import { XiaoMi } from './XiaoMi'
import { GameApp } from './GameApp'
import { MailApp } from './MailApp'

let iPhone = new IPhone(), xiaomi = new XiaoMi(), gameApp = new GameApp(), mailApp = new MailApp();

gameApp.start(iPhone);
gameApp.action(iPhone);
gameApp.end(iPhone);

mailApp.start(xiaomi);
mailApp.action(xiaomi);
mailApp.end(xiaomi);
