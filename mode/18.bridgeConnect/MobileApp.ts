/**
 * 桥接模式： 手机软件类
 */
import { MobileBrand } from './MobileBrand'

interface MobileApp{
	
	//开始运行
	start(brand:MobileBrand):void;
	//运行中
	action(brand:MobileBrand):void;
	//结束运行
	end(brand:MobileBrand):void;
}

export { MobileApp }
