/**
 * 软件： 游戏软件App
 */
import { MobileApp } from './MobileApp'
import { MobileBrand } from './MobileBrand'

class GameApp implements MobileApp{
	
	start(brand:MobileBrand):void{
		console.log(`${brand.getBrand()} 启动游戏`)
	};
	
	action(brand:MobileBrand):void{
		console.log(`${brand.getBrand()} 运行游戏中。。。`)
	};
	
	end(brand:MobileBrand):void{
		console.log(`${brand.getBrand()} 关闭游戏`)
	}
}

export { GameApp }
