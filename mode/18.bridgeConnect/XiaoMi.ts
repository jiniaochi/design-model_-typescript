/**
 * 手机品牌：小米
 */

import { MobileBrand } from './MobileBrand'

class XiaoMi implements MobileBrand{
	
	public getBrand():string{
		return '小米手机';
	};
}

export { XiaoMi }
