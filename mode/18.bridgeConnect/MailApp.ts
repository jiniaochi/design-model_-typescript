/**
 * 邮件App
 */
import { MobileApp } from './MobileApp'
import { MobileBrand } from './MobileBrand'

class MailApp implements MobileApp{
	
	start(brand:MobileBrand):void{
		console.log(`${brand.getBrand()} 启动邮箱App`)
	};
	
	action(brand:MobileBrand):void{
		console.log(`${brand.getBrand()} 运行邮箱App中。。。`)
	};
	
	end(brand:MobileBrand):void{
		console.log(`${brand.getBrand()} 关闭邮箱App`)
	}
}

export { MailApp }