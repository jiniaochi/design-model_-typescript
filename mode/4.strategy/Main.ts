/**
 * 策略模式： 面向 接口/基类 编程
 * 
 * 策略执行人 只对传入的 策略对象负责（形参为策略接口），不管传入的是策略A对象还是策略B对象，只要二者都实现了策略接口即可。
 */
import  { ActivityHolder } from './ActivityHolder'
import { ConcreteActivity_A } from './ConcreteActivity_A'
import { ConcreteActivity_B } from './ConcreteActivity_B'


let holder:ActivityHolder = new ActivityHolder();

holder.holdActivity(new ConcreteActivity_A('活动嘉年华'));
holder.holdActivity(new ConcreteActivity_B('水上欢乐园'));

