import {Activity} from './Activity'

class ConcreteActivity_A implements Activity{
	private activityName:string = 'activity';
	
	constructor(actName:string){
		console.log(`A活动被创建,活动名称由${this.activityName} 改为${actName}`);
		this.activityName = actName;
	}
	
	operateActivity():void{
		console.log('A活动被举办');
	}
}

export { ConcreteActivity_A }