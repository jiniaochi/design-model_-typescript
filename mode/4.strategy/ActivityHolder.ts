import { Activity } from './Activity';

class ActivityHolder{
	
	holdActivity(activity:Activity):void{
		activity.operateActivity();
	}
}

export { ActivityHolder }