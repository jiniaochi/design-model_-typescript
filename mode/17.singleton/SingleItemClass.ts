/**
 * 单例模式： ts的class实现
 */

import SingleItem from "./SingleItem";

export default class SingleItemClass implements SingleItem{
    remark: String = 'ts构造函数私有化';

    static sinleItem:SingleItem = new SingleItemClass();
    
    private constructor(){}

    public printSef():void{
        console.log(this.remark)
    }
}


