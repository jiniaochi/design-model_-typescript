/**
 * 单例模式：
 * 构建模式之一，相对于工厂模式生产多个相同或类似的对象，单例模式只产生一个对象
 * Java等严格OPP类型的语言，单例模式相对复杂，如构造函数private；
 * JavaScript直接声明一个对象即可，简单很多; 当然也可以使用ts的构造函数私有化方式拿到。
 */
import SingleItemClass from "./SingleItemClass";
import SingleItemObj from "./SingleItemObj";

SingleItemObj.printSelf()

SingleItemClass.sinleItem.printSef()

// 访问报错
// new SingleItemClass()
