import DataList from "./DataList";
import MyIterator from "./MyIterator";

export default class ConcreateIterator implements MyIterator{
    
    private dataList: DataList;
    private currentIndex:number = 0;
    private hasStarted:boolean = false;

    constructor(list:DataList){
        this.dataList = list
    }

    next() {
        if(this.hasStarted){
            if(this.hasNext){
                this.currentIndex++;
                return this.current()
            }

            return null
        }else{
            this.hasStarted = true;
            return this.current()
        }
    }
    hasNext(): boolean {
        if(this.currentIndex<this.dataList.geLen()-1){
            return true
        }
        return false
    }
    current() {
        if(this.currentIndex>=0&&this.currentIndex<this.dataList.geLen()){
            return this.dataList.getItem(this.currentIndex)
        }

        return null
    }

}
