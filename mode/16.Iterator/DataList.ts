
export default class DataList{

    private list:Array<any>;

    constructor(obj:Object){
        // 对象转为 一个个键值对的数组
        this.list = Object.entries(obj).map(([key,val])=>({ [key]:val }))
    }

    public geLen():number{
        return this.list.length
    }

    public getItem(index:number){
        if(index>=0&&index<this.geLen()){
            return this.list[index]
        }

        return null
    }
}
