/**
 * 迭代器模式: 
 * 提供一种方法按顺序访问 聚合对象(链表、数组、哈希表)的各个元素，
 * 而不暴露 聚合对象的内部表示（如数组，arr[1]就是暴露对象内部表示）
 */

import ConcreateIterator from "./ConcreateIterator";
import DataList from "./DataList";

const iterator = new ConcreateIterator(new DataList({ name:'dqwdwq',age:13, adddress:'dqdwqd', logo:'hello' }))

console.log( '==', iterator.next(),'==');
console.log( '==', iterator.next(),'==');
console.log( '==', iterator.next(),'==');
console.log( '==', iterator.next(),'==');
console.log( '==', iterator.next(),'==');
console.log( '==', iterator.next(),'==');
console.log( '==', iterator.next(),'==');
console.log( '==', iterator.next(),'==');
console.log( '==', iterator.next(),'==');



