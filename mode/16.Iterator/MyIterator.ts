

export default interface MyIterator{
    next():any;// 下一个 迭代对象
    hasNext():boolean; // 是否已经迭代完成
    current():any; // 当前迭代对象
}
