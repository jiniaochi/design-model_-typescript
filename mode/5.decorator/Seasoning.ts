import { Drinking } from './Drinking'

/**
 * 调料类: 准确的说是加 饮料后 的调料类
 */
abstract class Seasoning implements Drinking {
	
	//主饮料
	protected drinkOfMain:Drinking = null;
	 
	//设置主饮料 
	public setMainDrinking(mainDrinking:Drinking):void{
		this.drinkOfMain = mainDrinking;
	} 
	
	//开始喝
	public abstract drink():void;
	
}

export { Seasoning }