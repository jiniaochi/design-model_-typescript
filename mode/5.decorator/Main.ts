/**
 * 装饰者模式： 
 * 装饰者模式一般用来增强特定类的功能而不改写这个类的，
 * 如这个例子重点Coffee类，所以创建了一个抽象类 Seasoning 实现Drinking接口（因为要拓展Coffee类，其实继承Coffee类也是可行的，只是不符合面向接口的原则）
 * 然后用 Milk类/Surgar类（其实这两个类就是增强Coffee类的拓展类）继承Seasoning 并实现其中未实现的方法。
 * 
 * 总之 装饰着模式在不改变原类功能情况下，使用的是 继承和关联 关系； 
 * 我们日常中使用，只产生一个拓展类的情况下，继承并改写部分方法即可（使用super.xx()调用父类方法以增强）
 */
import { Surgar } from './Surgar'
import { Milk } from './Milk'
import { Coffee } from './Coffee'

let milk = new Milk(), surgar = new Surgar();
 
 milk.setMainDrinking(new Coffee());
 surgar.setMainDrinking(new Coffee());
 
 milk.drink();
 surgar.drink();


