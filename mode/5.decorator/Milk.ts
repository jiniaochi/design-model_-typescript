import { Seasoning } from './Seasoning'
/**
 * 调料--牛奶
 */
class Milk extends Seasoning{
	constructor(){
		console.log('牛奶类被初始化');
		super();
	}
	
	public drink():void{
		if(this.drinkOfMain == null){
			console.log('主饮料还没准备好');
		}else{
			console.log('加入牛奶，搅拌。。。');
			this.drinkOfMain.drink();
		}
	}
}

export { Milk }