/**
 * 饮品接口
 */
interface Drinking {
	drink():void;
}

export { Drinking }