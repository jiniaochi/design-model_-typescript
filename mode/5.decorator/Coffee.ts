import { Drinking } from './Drinking'

/**
 * 咖啡类--主饮料
 */ 
class Coffee implements Drinking {
	 
	 public drink():void{
		 console.log('开始喝咖啡');
	 }
 }
 
 export { Coffee }