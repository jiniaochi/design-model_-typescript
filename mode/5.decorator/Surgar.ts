import { Seasoning } from './Seasoning'

/**
 * 饮料--糖
 */
class Surgar extends Seasoning{
	
	constructor(){
		console.log('糖类被初始化');
		super();
	}
	
	public drink():void{
		if(this.drinkOfMain == null){
			console.log('主饮料还没准备好');
		}else{
			console.log('加入糖，搅拌。。。');
			this.drinkOfMain.drink();
		}
	}
}

export { Surgar }