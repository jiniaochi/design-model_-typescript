/**
 * 两空插座
 */

class TwoHoleSocket {
	
	constructor(){
		console.error(`双孔插座初始化完成`);
	}
	
	public powerOn():void{
		console.log(`双孔插座启动`);
	}
	
	public powerOff():void{
		console.log(`双孔插座停止`);
	}
}

export { TwoHoleSocket }  