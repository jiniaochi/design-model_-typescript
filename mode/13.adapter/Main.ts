/**
 * 适配器模式：
 * 让A类对象 有能够访问B类方法（但是A类和B类没有关系）的能力，如下面的例子中 使用双孔插座类 访问 三孔插座类的方法。
 * 本质上 是通过增加A类子类C，并让持有B类实例完成，使用的是 继承+关联 关系；因此本质上 适配器模式、装饰着模式、代理模式 是一样的，只是在语义上有所区别。
 * 让A类有访问C类的能力，换种说法也就是增加A类访问B类的能力，也就是A类功能增强。
 */
import { ThreeHoleSocket } from './ThreeHoleSocket'
import { TwoHoleSocket } from './TwoHoleSocket'
import { SocketAdapter } from './SocketAdapter'

let twoHoleAdpater:TwoHoleSocket =  new SocketAdapter(new ThreeHoleSocket());

twoHoleAdpater.powerOn();
twoHoleAdpater.powerOff();
