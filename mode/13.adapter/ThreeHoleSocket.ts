/**
 * 三孔插座
 */
class ThreeHoleSocket{
	
	constructor(){
		console.error(`三孔插座初始化完成`);
	}
	
	public powerOn():void{
		console.log(`三孔插座启动`);
	}
	
	public powerOff():void{
		console.log(`三孔插座关闭`);
	}
}

export { ThreeHoleSocket }
