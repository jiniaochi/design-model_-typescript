/**
 * 将双孔插座转换为三孔插座的适配器; 即双孔插座能调用三孔方法
 */

import { TwoHoleSocket } from './TwoHoleSocket'
import { ThreeHoleSocket } from './ThreeHoleSocket'


class SocketAdapter extends TwoHoleSocket{
	
	private threeHoleSocket:ThreeHoleSocket;
	
	constructor(_threeHoleSocket:ThreeHoleSocket){
		super();
		this.threeHoleSocket = _threeHoleSocket;
	}
	
	public powerOn():void{
		// doSomeThing
		this.threeHoleSocket.powerOn();
	}
	
	public powerOff():void{
		// doSomeThing
		this.threeHoleSocket.powerOff();
	}
	
}

export { SocketAdapter }
