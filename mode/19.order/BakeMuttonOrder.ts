/**
 * 烤羊肉串的订单
 */

import { Order } from './Order'
import { Barbecuer } from './Barbecuer'

class BakeMuttonOrder implements Order{
	
	private barbecuer:Barbecuer;
	
	setBarbecuer(barbecuer:Barbecuer):void{
		this.barbecuer = barbecuer;
	}
	
	action():void{
		this.barbecuer.bakeMutton();
	}
	
}

export { BakeMuttonOrder }
