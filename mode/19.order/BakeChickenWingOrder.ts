/**
 * 烤鸡翅的命令
 */

import { Order } from './Order'
import { Barbecuer } from './Barbecuer'

class BakeChickenWingOrder implements Order{
	
	private barbecuer:Barbecuer;
	
	setBarbecuer(barbecuer:Barbecuer):void{
		this.barbecuer = barbecuer;
	}
	
	action():void{
		this.barbecuer.bakeChickenWing();
	}
	
}

export { BakeChickenWingOrder }