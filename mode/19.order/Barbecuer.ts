/**
 * 厨师类： 接受 订单/命令 的类
 */

class Barbecuer {
	
	private name:string;
	
	constructor(name:string){
		this.name = name;
	}
	
	getName():string{
		return this.name;
	}
	
	bakeMutton():void{
		console.log(`厨师${this.name} 烤羊肉`);
	}
	
	bakeChickenWing():void{
		console.log(`厨师${this.name} 烤鸡翅`);
	}
	
}

export { Barbecuer }
