/**
 * 命令模式：发出命令的类和实现命令的类分离,用命令类做两者的连接;命令类/订单类 
 */

import { Barbecuer } from './Barbecuer'

interface Order{
	
	//做订单上的食物
	action():void;
	
	//设置订单的厨师
	setBarbecuer(barbecuer:Barbecuer):void;
	
}
 
export { Order }
