/**
 * 命令模式：
 * 将 命令或请求 封装成一个 抽象类，命令执行者只负责执行命令（不关心具体的命令子类），命令发布者负责管理命令的添加、删除和执行时机。
 * 有点类似策略模式，但是策略模式只有两个角色，即策略和策略执行人；
 * 但是命令模式有三个角色：命令、执行者、发布者/管理者
 */

import { BakeMuttonOrder } from './BakeMuttonOrder';
import { BakeChickenWingOrder } from './BakeChickenWingOrder';
import { Barbecuer } from './Barbecuer';
import { Waiter } from './Waiter';

let waiter = new Waiter('王晓红'), barbecuer = new Barbecuer('王刚');

//正式开店，顾客点餐
waiter.addCommand(new BakeMuttonOrder());
waiter.addCommand(new BakeChickenWingOrder());

waiter.startAction(barbecuer);

waiter.addCommand(new BakeMuttonOrder());
waiter.startAction(barbecuer);

