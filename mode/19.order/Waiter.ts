/**
 * 服务生类： 发出命令类
 */
import { Order } from './Order'
import { Barbecuer } from './Barbecuer'

class Waiter {
	
	private name:string;
	private commandList:Order[];
	
	public constructor(name:string){
		this.name = name;
		this.commandList = [];
	}
	
	public getName():string{
		return this.name;
	}
	
	public addCommand(order:Order):void{
		console.log(`服务员${this.name} 增加了订单`);
		this.commandList.push(order);
	}
	
	private clearCommand():void{
		console.log(`服务员${this.name} 清空了订单`);
		this.commandList.length = 0;
	}
	
	//让厨师开始做菜
	public startAction(barbecuer:Barbecuer){
		console.log(`服务员${this.name} 将订单列表交给了厨师${barbecuer.getName()}`);
		this.commandList.forEach(el=>{
			el.setBarbecuer(barbecuer);
			el.action();
		});
		
		this.clearCommand();
	}
	
}

export { Waiter }
