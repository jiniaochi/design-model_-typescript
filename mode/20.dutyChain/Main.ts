/**
 * 责任链模式： 将多个对象串成一个链表，请求在链表上传递执行；是适用于 链表数据结构的 设计模式
 */

import { GroupLeader } from './GroupLeader'

let groupLeader = new GroupLeader();

//请假2天
groupLeader.trial(2);

//请假4天
groupLeader.trial(4);

//请假8天
groupLeader.trial(8);
