/**
 * 责任链模式: 以请假为例，<3天组长审批，3-7天经理审批，7天以上老板审批
 */

abstract class Leader{
	
	//下一级审批者
	private nextLevel:Leader;
	
	public constructor(nextLevel:Leader|null){
		this.nextLevel = nextLevel; // 设置下一节点
	}
	
	public getNextLevel():Leader{
		return this.nextLevel;
	}
	
	public abstract trial(days:number):void;
	
}

export { Leader }
