/**
 * 组长类
 */

import { Leader } from './Leader'
import { Manager } from './Manager'

class GroupLeader extends Leader{
	
	public constructor(){
		super(new Manager()); // 设置下一节点
	}
	
	public trial(days:number):void{
		if(days<3){
			console.log('组长审批通过');
		}else{
			console.log('组长权限不够，转交经理');
			this.getNextLevel().trial(days);
		}
	}

}

export { GroupLeader }