/**
 * 老板类
 */

import { Leader } from './Leader'

class Boss extends Leader{
	
	public constructor(){
		super(null);// 没有下一节点
	}
	
	public trial(days:number):void{
		console.log('老板审批通过');
		
	}

}

export { Boss }
