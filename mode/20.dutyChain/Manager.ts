/**
 * 经理类
 */

import { Leader } from './Leader'
import { Boss } from './Boss'

class Manager extends Leader{
	
	public constructor(){
		super(new Boss()); // 设置下一节点
	}
	
	public trial(days:number):void{
		if(days<7){
			console.log('经理审批通过');
		}else{
			console.log('经理权限不够，转交老板');
			this.getNextLevel().trial(days);
		}
	}

}

export { Manager }