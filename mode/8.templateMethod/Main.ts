/**
 * 模板方法模式： 将不变的、统一的内容（方法或成员变量）放到基类/父类中，
 * 当然父类不一定需要是抽象类，这里因为没有使用父类的地方所以做成了抽象类，非抽象方法的空方法在子类重写也可以。
 */

import { PaperOfStudentA } from './PaperOfStudentA';
import { PaperOfStudentB } from './PaperOfStudentB';

new PaperOfStudentA().showAll();
new PaperOfStudentB().showAll();