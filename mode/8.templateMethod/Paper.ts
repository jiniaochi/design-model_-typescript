/**
 * 场景: 考试抄抄考题, 保证学生试卷一致，但是答案不一样
 * 抽象类: 考试试题
 */
abstract class Paper{
	
	public showAll():void{
		this.question_1();
		this.question_2();
		this.question_3();
	}
		
	public question_1():void{
		console.log('请输入你的名字:\t'+ this.answer_1());
	}
	
	public question_2():void{
		console.log('请输入你的年龄:\t' + this.answer_2());
	}
	
	public question_3():void{
		console.log('请输入你的地址\t' + this.answer_3());
	}
	
	public abstract  answer_1():string;
	
	public abstract  answer_2():number;
	
	public abstract  answer_3():string;
}

export { Paper }


