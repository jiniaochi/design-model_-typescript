/**
 * 学生A的试卷
 */
import { Paper } from './Paper'

class PaperOfStudentA extends Paper{
	
	public answer_1():string{
		return '狄更斯';
	}
	
	public answer_2():number{
		return 25;
	}
	
	public answer_3():string{
		return '大英帝国-雅各岛';
	}
}

export { PaperOfStudentA }
