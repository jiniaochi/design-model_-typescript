/**
 * 学生B的试卷
 */
import { Paper } from './Paper'

class PaperOfStudentB extends Paper{
	
	public answer_1():string{
		return '本杰明·富兰克林';
	}
	
	public answer_2():number{
		return 23;
	}
	
	public answer_3():string{
		return '新英格兰-波士顿';
	}
}

export { PaperOfStudentB }
