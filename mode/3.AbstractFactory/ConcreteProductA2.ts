import AbstractProductA from "./AbstractProductA";

export default class ConcreteProductA2 implements AbstractProductA {
    public usefulFunctionA(): string {
        return '产品A类型的 A2 系列被生产';
    }
}