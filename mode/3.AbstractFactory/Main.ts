/**
 * 抽象工厂模式与工厂方法模式类似，也是用于创建一系列相关的对象。
 * 不同之处在于，抽象工厂是针对多个产品族而言的，即每个工厂可以创建多种不同类型的产品; 
 * 简单的说是抽象工厂的返回值是 基类/接口，导致抽象工厂不止可以返回同一个 基类/接口 的多种子类。
 * 
 * 这样的话，抽象工厂为创建一组相关或独立的对象提供了一种方式。
 */
import AbstractFactory from "./AbstractFactory";
import ConcreteFactory1 from "./ConcreteFactory1";
import ConcreteFactory2 from "./ConcreteFactory2";

function clientCode(factory: AbstractFactory) {
    const productA = factory.createProductA();
    const productB = factory.createProductB();

    console.log(productB.usefulFunctionB());
    console.log(productB.anotherUsefulFunctionB(productA));
}

clientCode(new ConcreteFactory1());
clientCode(new ConcreteFactory2());


