import AbstractProductA from "./AbstractProductA";

export default class ConcreteProductA1 implements AbstractProductA {
    public usefulFunctionA(): string {
        return '产品A类型的 A1 系列被生产';
    }
}