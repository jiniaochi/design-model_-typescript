/**
 * 角色类--持有状态
 */
import { State } from './State'
import { StateManager } from './StateManager'

export default class Role{
	
	private state:State;
	private stateManager:StateManager;
	
	public constructor(){
		this.state = new State();
		this.stateManager = new StateManager();
	}
	
	//用户角色 陷入危险
	public toDanger():void{
		this.state = new State();
		this.state.setHp(5);
		this.state.setXp(20);
		console.log(`角色陷入危险!!`);
	}
	
	//用户角色 通关
	public toSucess():void{
		this.state = new State();
		this.state.setHp(75);
		this.state.setXp(100);
		console.log(`角色通关!!`);
	}
	
	//用户角色 存档
	public saveState():void{
		console.log('角色状态存档');
		this.stateManager.pushState(this.state);
	}
	
	//用户角色 回退
	public rollBackState():void{
		console.log(`用户角色读档`);
		this.state = this.stateManager.getLastState();
	}
	
	public tellState():void{
		console.log(`目前状态,生命值为${this.state.getHp()},经验值为${this.state.getXp()}`)
	}
}

