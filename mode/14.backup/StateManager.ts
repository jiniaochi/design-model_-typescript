/**
 *  角色状态管理器/存档器
 */
import { State } from './State'

class StateManager{
	
	private stateList:State[] = [];
	
	public pushState(_state:State):void{
		this.stateList.push(_state);
	}
	
	//返回前preCount次的状态;
	public getState(preCount:number):State{
		return this.stateList[ this.stateList.length-1- preCount]?
		this.stateList[ this.stateList.length-1- preCount]:
		this.stateList[ this.stateList.length]
	}
	
	//返回最后一次的角色状态
	public getLastState():State{
		return this.stateList[this.stateList.length - 1 ];
	}
}

export { StateManager }
