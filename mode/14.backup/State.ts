/**
 * 状态类--包括 用户hp生命值，xp经验值
 */
class State{
	
	private hp:number = 100;
	private xp:number = 0;
	
	getHp():number{
		return this.hp;
	}
	
	getXp():number{
		return this.xp;
	}
	
	setHp(_hp:number):void{
		this.hp = _hp;
	}
	
	setXp(_xp:number):void{
		this.xp = _xp;
	}
}

export { State }
