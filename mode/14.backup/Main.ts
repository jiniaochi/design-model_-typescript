/**
 * 备忘录（存档 读档模式） 模式：
 * 
 * 状态管理类 管理 用户所用状态（一般通过队列），并被用户持有；
 * 用户需要的情况下，主动通过 状态管理类 获取上一状态，并设置目前状态为上以状态，比较常见的就是游戏存档 读档。
 */
import Role from './Role'

let user = new Role();
user.tellState();
user.saveState();
user.toDanger();
user.tellState();

user.rollBackState();
user.tellState();



