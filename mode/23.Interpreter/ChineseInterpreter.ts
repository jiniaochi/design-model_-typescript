import Interpreter from "./Interpreter";

const dict: { [key: string]: string } = {
    'i':'我',
    'you': '你',
    'tell': '告诉',
    'hello': '你好',
    'that':' ',
    'world': '世界',
    'unkown': '未知词语',
}

export default class ChineseInterpreter implements Interpreter{
    interprete(rawStr: string, context: Object): string {
        let resultArr:Array<string> = []
        if(rawStr.length){
            const rawArr = rawStr.split(/\s/g);
            resultArr  = rawArr.map( str => dict[str]||dict.unkown )
        }

        return resultArr.join('');
    }
    
}

