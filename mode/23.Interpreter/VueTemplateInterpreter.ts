import Interpreter from "./Interpreter";


export default class VueTemplateInterpreter implements Interpreter{

    interprete(rawStr: string, context: { [key:string]: any }|null): string {
        if(!rawStr.length||!context){
            return ''
        }

        // 分组精装匹配
        return rawStr.replace(/{{(\w+)}}/ig, function(matchStr,group1){
            return context[group1]
        })

    }
    
}
