
export default interface Interpreter{
    interprete(rawStr:string, context:{ [key:string]: any }|null):string;
}

