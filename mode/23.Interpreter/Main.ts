import ChineseInterpreter from "./ChineseInterpreter";
import Interpreter from "./Interpreter";
import VueTemplateInterpreter from "./VueTemplateInterpreter";

/**
 * 解释器模式：
 * 将一种类型的 语言/字符串，解释为另一种 语言/字符串；例如英文解释为中文，vue的模板解释为html（如下）
 */
const chineseTransform:Interpreter = new ChineseInterpreter()

console.log( chineseTransform.interprete('i tell you that hello world', null) )

console.log( `============= 分割线 =============` );

const vueTemplate = new VueTemplateInterpreter()

console.log( vueTemplate.interprete(`I tell a story about {{tom}}`, { tom: 'Tom Simith-J ' }) )



