/**
 * 通知者接口
 */
import { Observer } from './Observer'

interface Subject{
	
	//提醒单个(被)观察者开始行动
	notifyStart(observer:Observer):void;
	
	//提醒单个(被)观察者终止行动
	notifyEnd(observer:Observer):void;
	
	//提醒所有（被）观察者终止行动
	notityAllStart():void;
	
	//提醒所有（被）观察者终止行动 
	notityAllEnd():void;
	
	//添加观察者
	addObserver(ob:Observer):void;
	
	//移除观察者
	removeObserver(ob:Observer):void;
}

export { Subject }