/**
 * 观察者模式也叫订阅-通知模式：主题者Subject持有观察者Observer的对象引用，主题者主动提示观察者产生对应的行为; 其重点在主题者而非观察者
 * 
 * 被观察者 类
 */

interface Observer {
	//(被)观察者开始行动
	start():void;
	
	//(被)观察者停止行动
	end():void;
	
	//返回被观察者的姓名
	getName():string;
}

export { Observer }