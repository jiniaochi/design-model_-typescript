/**
 * 具体的观察主题
 */

import { Subject } from './Subject'
import { Observer } from './Observer'

class ConcreteSubject implements Subject {
	
	private subjectName:string;
	private observerMap:Map<string,Observer>;
	
	constructor(_name:string){
		this.subjectName = _name;
		this.observerMap = new Map();
	}
	
	addObserver(ob:Observer):void{
		this.observerMap.set(ob.getName(),ob);
	}
	
	removeObserver(ob:Observer):void{
		this.observerMap.delete(ob.getName());
	}
	
	notifyStart(ob:Observer):void{
		console.log(`${ this.subjectName } 老板偷偷告诉 ${ ob.getName() } 今天要加班`);
		ob.start();
	}
	
	notifyEnd(ob:Observer):void{
		console.log(`${ this.subjectName } 老板偷偷告诉 ${ ob.getName() } 今天可以提前下班`);
		ob.end();
	}
	
	notityAllStart():void{
		console.log(`${ this.subjectName } 发布上班公告`);
		for( let ob of this.observerMap.values() ){
			ob.start();
		}
	}
	
	notityAllEnd():void{
		console.log(`${ this.subjectName } 发布下班公告`);
		for( let ob of this.observerMap.values() ){
			ob.end();
		}
	}
	
}

export { ConcreteSubject }
