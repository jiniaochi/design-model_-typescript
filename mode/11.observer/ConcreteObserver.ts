/**
 * 具体的观察者
 */
import { Observer } from './Observer'

class ConcreteObserver implements Observer {
	
	private obName:string;
	
	public getName(){
		return this.obName;
	}
	
	public constructor(_obName:string){
		this.obName = _obName;
	}
	
	public start():void {
		console.log(`观察者 ${this.obName} 开始了搬砖`);
	}
	
	public end():void {
		console.log(`观察者 ${this.obName} 停止了搬砖`);
	}
	
}

export { ConcreteObserver } 