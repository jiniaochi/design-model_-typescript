/**
 * 观察者模式（也叫 发布订阅模式）： 观察者定义了一种 一对多的关系，主题变化时主动通知订阅人列表。
 * 
 * 主题对象 维护一个订阅人队列(这里使用的时Map结构)，主题对象 变化时依次遍历队列通知 订阅人； 注意这里主动通知能力的是主题，订阅人是被动接受的。
 */

import { ConcreteSubject } from './ConcreteSubject' 
import { ConcreteObserver } from './ConcreteObserver'
 
let subject  = new ConcreteSubject(`肉联加工厂`),
	work_1 = new ConcreteObserver(`工人小王`),
	work_2 = new ConcreteObserver(`工人小红`),
	work_3 = new ConcreteObserver(`工人小明`);
	 
subject.addObserver(work_1);
subject.addObserver(work_2);
subject.addObserver(work_3);

subject.notityAllStart();
subject.notityAllEnd(); 

subject.notifyStart(work_2);
subject.notifyEnd(work_2);

 