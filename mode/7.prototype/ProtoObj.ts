import { ProtoType } from './ProtoType'
  
class ProtoObj implements ProtoType{
	private objName:string;
	private objValue:number;
	public constructor(_objName:string, _objValue:number){
		this.objName = _objName;
		this.objValue = _objValue;
	}
	
	public clone():ProtoType {
		return new ProtoObj(this.objName,this.objValue);
	}
	
	public toString():void{
		console.log(` the name is ${this.objName},and the value is ${this.objValue} `)
	}
}
  
export { ProtoObj }  