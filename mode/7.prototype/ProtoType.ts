/**
 * 原型接口
 */
interface ProtoType{
	clone():ProtoType;
	toString():void;
}

export  { ProtoType }
