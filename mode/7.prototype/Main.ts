/**
 * 原型模式： 也叫复制模式，是使用一个对象 自我复制的方式创建 另外一个对象的模式（当然这里的复制分为 浅复制和深复制）
 */
import { ProtoType } from './ProtoType'
import { ProtoObj } from './ProtoObj'

let protoObject = new ProtoObj('俾斯麦',58);

const objA:ProtoType =  protoObject.clone()
const objB:ProtoType =  protoObject.clone()
const objC:ProtoType =  protoObject.clone()

console.log( objA,objB,objC );
console.log( objA==objB,objB==objC );

