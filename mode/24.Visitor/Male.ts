/**
 * 男人类
 */
import { Person } from './Person'

class Male extends Person{
	
	public constructor(){
		super();
		this.gender = 'male';
	}
	
}

export { Male }
