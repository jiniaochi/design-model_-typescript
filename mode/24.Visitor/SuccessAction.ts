/**
 * 成功的行为
 */
import { Action } from './Action'
import { Person } from './Person'

class SuccessAction extends Action{
	
	public constructor(){
		super();
		this.actionName = '成功';
	}
	
	perform(person:Person):void{
		if(person.getGender() == 'male'){
			console.log(`男人${this.actionName}时，背后大多有一个伟大的女人。`);
		}else if(person.getGender() == 'female'){
			console.log(`女人${this.actionName}时，背后大多有一个不成功的男人。`);
		}
	}
	
}

export { SuccessAction }
