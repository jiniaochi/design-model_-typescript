/**
 * 女人类
 */
import { Person } from './Person'

class Female extends Person{
	
	public constructor(){
		super();
		this.gender = 'female';
	}
	
}

export { Female }

