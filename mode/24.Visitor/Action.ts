/**
 * 行为类: 包括成功、失败和两个类
 */
import { Person } from './Person'

abstract class Action{
	
	protected actionName:string;
	
	abstract perform(person:Person):void;
	
}

export { Action }
