/**
 * 失败的行为
 * 
 */
import { Action } from './Action'
import { Person } from './Person'

class FailAction extends Action{
	
	public constructor(){
		super();
		this.actionName = '失败';
	}
	
	perform(person:Person):void{
		if(person.getGender() == 'male'){
			console.log(`男人${this.actionName}时，背后喝闷酒，谁也不用劝。`);
		}else if(person.getGender() == 'female'){
			console.log(`女人${this.actionName}时，眼泪汪汪，求安慰。`);
		}
	}
	
}

export { FailAction }