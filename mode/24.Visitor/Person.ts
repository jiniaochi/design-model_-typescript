/**
 * 人类接口
 */

import { Action } from './Action'

abstract class Person{
	
	protected gender:string;
	protected action:Action;
	
	public show():void{
		if(this.action){
			this.action.perform(this);
		}else{
			console.log('请先设置行为类action');
		}
		
	}
	
	public getGender():string{
		return this.gender;
	}
	
	public setAction(action:Action){
		this.action = action;
	}
	
	
}

export { Person }