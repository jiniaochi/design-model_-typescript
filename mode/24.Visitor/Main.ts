
/**
 * 访问者模式： 
 * 将 访问者 的角色类（如访问者A类，访问者B类）和 行为类（如下面的成功操作、失败操作） 相分离；
 * 将这些类分组（如角色组、行为组），然后对各个组内细化，最后桥接 两个组。
 */

import { Male } from './Male'
import { Female } from './Female'
import { SuccessAction } from './SuccessAction'
import { FailAction } from './FailAction'

let male = new Male(),female = new Female(),
	sucess = new SuccessAction(),fail = new FailAction();

male.setAction(sucess);
male.show();
female.setAction(sucess);
female.show();

console.log('----------华丽的分割线----------');

male.setAction(fail);
male.show();
female.setAction(fail);
female.show();
