/**
 * 系统B 类
 */

class SystemTwo {
	private systemName:string;
	
	public constructor(_sysName:string){
		this.systemName = _sysName;
	}
	
	public powerOn():void{
		console.log(`系统B--${this.systemName} 开机`);
	}
	
	public powerOff():void{
		console.log(`系统B--${this.systemName} 关机`);
	}
}

export { SystemTwo }