/**
 * 外观模式： 将各个模块的协调封装在一起的模式
 * 将各个 模块/系统 的操作封装到一起，形成一个外观类，如 做饭= 开火+放油+放主食+加佐料+加小料，这些步骤散开很难维护（操作繁多且要注意顺序），
 * 就把这些 功能聚合为一个外观类，统一操作， 在外面开起来只简单执行了 做菜方法。
 */
import { SystemOne } from './SystemOne'
import { SystemTwo } from './SystemTwo'
import { Facade } from './Facade'

let facade =  new Facade(new SystemOne('北京的系统'),new SystemTwo('好望角的系统'));
facade.start();
facade.end();

