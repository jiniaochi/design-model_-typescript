/**
 * Facade --英文为外观、表面 
 * 外观模式
 */
import { SystemOne } from './SystemOne'
import { SystemTwo } from './SystemTwo'

class Facade{
	private systemOne:SystemOne;
	private systemTwo:SystemTwo;
	
	constructor(_systemOne:SystemOne,_systemTwo:SystemTwo){
		this.systemOne = _systemOne;
		this.systemTwo = _systemTwo;
	}
	
	public start():void{
		this.systemOne.powerOn();
		this.systemTwo.powerOn();
	}
	
	public end():void{
		this.systemOne.powerOff();
		this.systemTwo.powerOff();
	}
}

export { Facade }