/**
 * 系统A 类
 */

class SystemOne {
	private systemName:string;
	
	public constructor(_sysName:string){
		this.systemName = _sysName;
	}
	
	public powerOn():void{
		console.log(`系统A--${this.systemName} 启动`);
	}
	
	public powerOff():void{
		console.log(`系统A--${this.systemName} 关闭`);
	}
}

export { SystemOne }