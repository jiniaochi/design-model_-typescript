/**
 * 享元类 接口
 */

interface FlyWeight {
	
	action():void;
	
}

export { FlyWeight }
