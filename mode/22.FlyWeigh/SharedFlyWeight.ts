/**
 * 分享、多次使用的对象
 */

import { FlyWeight } from './FlyWeight'

class SharedFlyWeight implements FlyWeight{
	
	private name:string;
	
	constructor(name:string){
		this.name = name;
	}
	
	action():void{
		console.log(`共享对象的Action执行，对象名称为 ${ this.name }`);
	}
	
}

export { SharedFlyWeight }
