/**
 * 享元模式： 池化技术的核心思想，对于已经创建了过了的对象，再次创建时 直接使用之前的对象; 可以说是进阶版的 工厂模式。
 * 
 */
import { FlyWeightFactory } from './FlyWeightFactory'

let sharedObj1 = FlyWeightFactory.getFlyWeightObj( true,'share1'),
	sharedObj2 = FlyWeightFactory.getFlyWeightObj( true,'share2'),
	unSharedObj3 = FlyWeightFactory.getFlyWeightObj( false,'share3');

sharedObj1.action();
sharedObj2.action();
unSharedObj3.action();


