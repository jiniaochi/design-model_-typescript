/**
 * 非共享 对象
 */

import { FlyWeight } from './FlyWeight'

class UnSharedFlyWeight implements FlyWeight{
	
	private name:string;
	
	constructor(name:string){
		this.name = name;
	}
	
	action():void{
		console.log(`非--共享对象的Action执行，对象名称为 ${ this.name }`);
	}
	
}

export { UnSharedFlyWeight }
