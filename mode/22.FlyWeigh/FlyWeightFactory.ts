/**
 * 享元 工厂
 */

import { FlyWeight } from './FlyWeight'
import { SharedFlyWeight } from './SharedFlyWeight'
import { UnSharedFlyWeight } from './UnSharedFlyWeight'

class FlyWeightFactory {
	
	private static sharedList:Map<string,SharedFlyWeight> = new Map();
	
	public static getFlyWeightObj(isShare:boolean,key:string):FlyWeight{
		if(isShare){
			if(!FlyWeightFactory.sharedList.has(key)){
				FlyWeightFactory.sharedList.set(key,new SharedFlyWeight(key));
			}
			return FlyWeightFactory.sharedList.get(key);
		}else{
			return new UnSharedFlyWeight(key);
		}
	}
	
}

export { FlyWeightFactory }
