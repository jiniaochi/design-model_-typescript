/**
 * 代理模式： 作用是为了间接性访问真实对象（真实对象并非不工作，也需要工作），只是在这层间接性中我们可以做一些事，例如用户鉴权等
 * 
 * 我们仔细分析，会发现 装饰着模式、代理模式本质上都是将实际运用的对象进行了再一次封装，而且二者都是通过成员变量的方式存放实际对象；
 * 二者区别只停留在语义上或者说业务作用上，即 我们需要对一个类进行功能增强 一般叫装饰者模式；而我们需要对一个控制类进行控制访问，一般叫 代理模式
 * 
 * 类似语义化在《GOF23设计模式》中很明显，例如 代理模式和策略模式。
 */
import { RealBoss } from './RealBoss'
import { ProxyBoss } from './ProxyBoss'

let realBoss = new RealBoss('丁原英'), proxyBoss = new ProxyBoss('董建明',realBoss);
 
 proxyBoss.action();
