import { Boss } from './Boss'

/**
 * 真正的老板--只做核心的东西
 */
class RealBoss implements Boss{
	private name:string = 'real boss';
	
	constructor(_name:string){
		this.name = _name;
	}
	
	action():void{
		console.log(`真正老板${this.name}开始行动，着手核心性质事务`);
	}
}

export { RealBoss }