import { Boss } from './Boss'

/**
 * 代理老板--外包，做一些辅助性的东西
 */
class ProxyBoss implements Boss{
	
	private name:string = 'fake Boss';
	private realBoss:Boss = null;
	
	constructor(_name:string, _realBoss:Boss){
		this.name = _name;
		this.realBoss = _realBoss;
	}
	
	action():void{
		console.log(`代理/外包 老板${this.name}开始行动，着手辅助性质事务`);
		this.realBoss.action();
	}
	
}

export { ProxyBoss }
