import { AddCal } from './AddCal';
import { SubCal } from './SubCal';
import { Calculate } from './Calculate';

class CalculateFactory{
	
	constructor(){
		console.log('simple factory has been created');
	}
	
	getCalculateObject(type:string):Calculate|undefined{
		let obj:Calculate|undefined = undefined;
		switch(type){
			case 'add': obj = new AddCal();break;
			case 'sub': obj = new SubCal();break;
			default:;
		}
		return obj;
	}
}

export { CalculateFactory };
