/**
 * 简单工厂模式：
 * 通过一个 工厂类/工厂方法，根据输入的参数，生成特定类别对象
 * 
 */
import { CalculateFactory } from './CalculateFactory';

let factory = new CalculateFactory(), 
	addCal = factory.getCalculateObject('add'),
	subCal = factory.getCalculateObject('sub');

addCal.operate(12,24);
subCal.operate(10,19);


