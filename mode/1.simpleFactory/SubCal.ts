import { Calculate } from './Calculate';

class SubCal implements Calculate {
	
	constructor(){
		console.log(`减法法对象被创造`);
	}
	
	operate(num1:number,num2:number):number{
		let res:number = num1 - num2;
		console.log(`相减结果为 ${res}`);
		return res;
	}
}

export {SubCal};
	
